#!/bin/bash

# Check if version parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <new-version>"
  exit 1
fi

# New version
NEW_VERSION=$1

function update_package() {
  # Path to package.json (assuming it's in the current directory)
  PACKAGE_JSON="./package.json"

  # Update the version in package.json
  if [ -f "$PACKAGE_JSON" ]; then
    # Use jq to update version (make sure jq is installed)
    jq --arg new_version "$NEW_VERSION" '.version = $new_version' "$PACKAGE_JSON" > tmp.$$.json && mv tmp.$$.json "$PACKAGE_JSON"
    echo "[${PACKAGE_JSON}] Version updated to $NEW_VERSION in $PACKAGE_JSON"
  else
    echo "Error: $PACKAGE_JSON not found!"
    exit 1
  fi
}

function update_kustomization() {
  KUSTOMIZATION_YAML="./base/kustomization.yaml"
  IMAGE_NAME="registry.gitlab.com/mike-ensor/price-a-tray-dynamic-frontend/dynamic-frontend"

  # Update the image tag in kustomization.yaml
  if [ -f "$KUSTOMIZATION_YAML" ]; then
    # Use yq to update the image tag (make sure yq is installed)
    yq -i "(.images[] | select(.name == \"$IMAGE_NAME\")).newTag = \"$NEW_VERSION\"" "$KUSTOMIZATION_YAML"
    echo "[${KUSTOMIZATION_YAML}] Image $IMAGE_NAME updated to tag $NEW_VERSION in $KUSTOMIZATION_YAML"
  else
    echo "Error: $KUSTOMIZATION_YAML not found!"
    exit 1
  fi
}

function update_base() {
  # use sed to replace the version
  ENV_VARS="./base/env-vars"

  if [ -f "$ENV_VARS" ]; then
    # Use sed to replace the value of PUBLIC_APP_VERSION
    sed -i.bak -E "s/(PUBLIC_APP_VERSION=).*/\1$NEW_VERSION/" "$ENV_VARS" && rm "$ENV_VARS.bak"
    echo "[${ENV_VARS}] PUBLIC_APP_VERSION updated to $NEW_VERSION in $ENV_VARS"
  else
    echo "Error: $ENV_VARS not found!"
    exit 1
  fi

}

function update_envrc() {
  # use sed to replace the version
  ENV_VARS=".envrc"

  if [ -f "$ENV_VARS" ]; then
    # Use sed to replace the value of PUBLIC_APP_VERSION
    sed -i.bak -E "s/(export VERSION=).*/\1$NEW_VERSION/" "$ENV_VARS" && rm "$ENV_VARS.bak"
    echo "[${ENV_VARS}] .envrc Version updated to $NEW_VERSION in $ENV_VARS"
    direnv allow . || true
  else
    echo "Note: '$ENV_VARS' not found. Skipping update of .envrc"
  fi

}

# 4 files need to be updated
update_package
update_kustomization
update_base
update_envrc
