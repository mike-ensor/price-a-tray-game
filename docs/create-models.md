# Overview

The process for creating or updating a computer vision model, like the ones used in Price a Tray is outlined
in the project: https://gitlab.com/mike-ensor/vertex-ai-computer-vision-helper


## LabelStudio

1. For each type of item (classifier) create a label name
1. Create the LabelStudio image setup simlar to below

```xml
<View>
  <Image name="sushi" value="$image" zoomControl="true" zoom="true"/>
  <RectangleLabels name="label" toName="sushi" choice="multiple">
    <Label value="background"/>
    <Label value="clam"/>
    <Label value="clear-fish"/>
    <Label value="corn-roll"/>
    <Label value="cucumber-roll"/>
    <Label value="ebi-artic"/>
    <Label value="ebi-atlantic"/>
    <Label value="ebi-pacific"/>
    <Label value="eel"/>
    <Label value="empty"/>
    <Label value="hokkigai"/>
    <Label value="oyster"/>
    <Label value="redfin"/>
    <Label value="saba"/>
    <Label value="sake"/>
    <Label value="sake-row"/>
    <Label value="tamago"/>
    <Label value="tuna-roll"/>
    <Label value="uni"/>
  </RectangleLabels>
</View>
```

