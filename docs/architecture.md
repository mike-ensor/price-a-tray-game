# Overview

Price-a-Tray Frontend is the primary user experience, but is also integrated with multiple other containers.

## Diagram

![Architecture Diagram](./architecture-diagram-software-white.png)