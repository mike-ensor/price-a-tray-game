import {sveltekit} from '@sveltejs/kit/vite';
import {defineConfig} from 'vitest/config';
import svg from '@poppanator/sveltekit-svg'
import {enhancedImages} from '@sveltejs/enhanced-img';

export default defineConfig({
    server: {
        fs: {
            // Allow serving files from one level up to the project root
            allow: ['..'],
        }
    },
    plugins: [
        sveltekit(),
        enhancedImages(),
        svg({
            includePaths: ['./src/lib/icons/', './src/assets/icons/'],
            svgoOptions: {
                multipass: true,
                plugins: [
                    {
                        name: 'preset-default',
                        // by default svgo removes the viewBox which prevents svg icons from scaling
                        // not a good idea! https://github.com/svg/svgo/pull/1461
                        params: {overrides: {removeViewBox: false}},
                    },
                    {name: 'removeAttrs', params: {attrs: '(fill|stroke)'}},
                ],
            },
        }),
        svg({
            includePaths: ['./src/lib/graphics/'],
            svgoOptions: {
                multipass: true,
                plugins: ['preset-default'],
            },
        }),
    ],
    test: {
        include: ['src/**/*.{test,spec}.{js,ts}']
    }
});
