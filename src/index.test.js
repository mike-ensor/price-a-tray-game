import { describe, it, expect } from 'vitest';

import {ordinal} from "$lib/constants.js";

describe('ordinal tests', () => {
	let stPlaces = [1,21,31,41,51,61,71,81,91];
	for(let i=0; i<stPlaces.length; i++) {
		const numb = stPlaces[i];
		it(`${numb} place is ${numb}st place`, () => {
			expect(`${numb}st`).eq(ordinal(numb));
		});
	}

	let ndPlaces = [2,22,32,42,52,62,72,82,92];

	for(let i=0; i<ndPlaces.length; i++) {
		const numb = ndPlaces[i];
		it(`${numb} place is ${numb}nd place`, () => {
			expect(`${numb}nd`).eq(ordinal(numb));
		});
	}

	let rdPlace = [3,23,33,43,53,63,73,83,93];
	for(let i=0; i<rdPlace.length; i++) {
		const numb = rdPlace[i];
		it(`${numb} place is ${numb}rd place`, () => {
			expect(`${numb}rd`).eq(ordinal(numb));
		});
	}

	it('3 place is 3rd place', () => {
		expect("3rd").eq(ordinal(3));
	});

	for(let i=4; i<21; i++) {
		it(`${i} place is ${i}th place`, () => {
			expect(`${i}th`).eq(ordinal(i));
		});
	}
});
