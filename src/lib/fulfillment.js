import { writable } from 'svelte/store';

export const identifiedItemsStore = writable([]);

export function updateItems(list) {
    identifiedItemsStore.set(list);
}
