import {appEnvs} from "$lib/appEnvs.js";

export function getGameItems(fetch, gameModelId) {
    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/game-model/${gameModelId}/game-objects`;
    const result = fetch(url);

    let gameItemsReturned = new Map();
    result.then((response) => {
        response.json().then((gameItemsOrig) => {
            // Convert GameItems to a hashmap of label-based keys
            let i = 0;

            gameItemsOrig.forEach((gameItem) => {
                gameItemsReturned.set(gameItem.model_label, {
                    id: gameItem.id,
                    name: gameItem.name,
                    lower: gameItem.lower,
                    upper: gameItem.upper,
                    image_link: gameItem.image_link,
                    color_index: i++
                });
            });
        });
    });

    return gameItemsReturned;
}

export async function getGameItemsBlocking(gameModelId) {
    let gameItemsReturned = new Map();

    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/game-model/${gameModelId}/game-objects`;
    const result = await fetch(url);

    let gameItems = await result.json();

    for (let index = 0; index < gameItems.length; index++) {

        let gameItem = gameItems[index];
        let i = 0;

        gameItemsReturned.set(gameItem.model_label,
            {
                id: gameItem.id,
                name: gameItem.name,
                lower: gameItem.lower,
                upper: gameItem.upper,
                image_link: gameItem.image_link,
                color_index: i++
            }
        );
    }

    return gameItemsReturned;
}