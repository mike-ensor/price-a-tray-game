import { browser } from '$app/environment'
import { init, register } from 'svelte-i18n'

const defaultLocale = 'en-US'

register('en', () => import('$lib/locales/en.json'))
register('en-US', () => import('$lib/locales/en.json'))
register('ja', () => import('$lib/locales/jp.json'))
register('jp', () => import('$lib/locales/jp.json'))
register('ja-JP', () => import('$lib/locales/jp.json'))

init({
    fallbackLocale: defaultLocale,
    initialLocale: browser ? window.navigator.language : defaultLocale,
})