// place files you want to import through the `$lib` alias in this folder.
import {appEnvs} from '$lib/appEnvs.js';

export async function getActiveGame(fetch) {
    try {
        const activeGameUrl = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/game/active`;
        const activeGameResult = await fetch(activeGameUrl);

        if (!activeGameResult.ok) {
            return {
                id: "NONE",
                name: "No Active Game",
                scoresRecorded: 0
            }
        } else {
            const activeGame = await activeGameResult.json();

            const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/game-scores-count/${activeGame.id}`;
            const recordsResult = await fetch(url);

            if(recordsResult.ok) {
                const counted = await recordsResult.text();
                return {
                    id: activeGame.id,
                    name: activeGame.name,
                    scoresRecorded: counted
                };
            } else {
                return {
                    id: activeGame.id,
                    name: activeGame.name,
                    scoresRecorded: 0
                };
            }
        }
    } catch (error) {
        if (error.name === 'TypeError') {
            console.error('Fetch failed:', error);
        } else {
            console.error('Error fetching data:', error);
        }

        return {
            id: "NONE",
            name: "No Active Game",
            scoresRecorded: 0
        }
    }
}


