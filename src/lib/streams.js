import {appEnvs} from "$lib/appEnvs.js";

export const setRTSPUrl = async () => {
    const urlObject = document.getElementById("rtspNewUrl");
    let newUrl = urlObject.value;
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/set-url?url=${newUrl}`;
    const res = await fetch(url);
    return await res.json();
}

export const getRTSPUrl = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/get-url`;
    const res = await fetch(url);
    return await res.json();
}

export const streamStatus = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/status`;
    const res = await fetch(url);
    return await res.json();
}

export const toggleStream = async (toggleState) => {
    // Returns a promise
    let status = streamStatus();
    status.then((response) => {
        if (response.state === false) {
            startStream()
            toggleState = true;
        } else {
            stopStream()
            toggleState = false;
        }
    }).catch((error) => {
        console.log(error);
        toggleState = false;
    });
}

export const startStream = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/start`;
    const res = await fetch(url);
    const rtspStatus = await res.json();
    return rtspStatus.state;
}

export const stopStream = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/stop`;
    const res = await fetch(url);
    const rtspStatus = await res.json();
    return rtspStatus.state;
}
