import {appEnvs} from "$lib/appEnvs.js";

export var defaultGameServerData = { app_version: '0.0.0', migrations: [] };

export const gameServerMetadata = async () => {
    const url = `${appEnvs.GAME_API_URI}/metadata`;
    const res = await fetch(url);
    return await res.json();
}

export const gameAPIHealth = async () => {
    const url = `${appEnvs.GAME_API_URI}/health`;
    const res = await fetch(url);
    return await res.json();
}

