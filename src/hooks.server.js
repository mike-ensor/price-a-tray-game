import {INITIALIZED, setInitialized} from '$lib/constants';
import {appEnvs} from '$lib/appEnvs.js';
import {Handle} from '@sveltejs/kit'
import {locale} from 'svelte-i18n'
import '$lib/i18n'

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({event, resolve}) {
    if (!INITIALIZED) {
        console.log("Startup sequence initiated.");
        console.log("=====================================================================");
        console.log("GAME_API:\thttp://" + appEnvs.GAME_API_HOST + ":" + appEnvs.GAME_API_PORT);
        console.log("RTSP_HOST:\thttp://" + appEnvs.RTSP_HOST + ":" + appEnvs.RTSP_PORT);
        console.log("INFERENCE_HOST:\thttp://" + appEnvs.INFERENCE_HOST + ":" + appEnvs.INFERENCE_PORT);
        console.log("INFERENCE_FPS:\t" + appEnvs.INFERENCE_FPS + " fps, " + 1000/appEnvs.INFERENCE_FPS + " milliseconds intervals");
        console.log("=====================================================================");
        setInitialized();
    }

    // Cookie or Header or Something

    if (! event.url.pathname.startsWith('/health')) {
        const urlParams = new URLSearchParams(event.url.searchParams);
        let foundLocale = "en"; // default

        // Get a specific cookie value
        const cookieLocale = event.cookies.get('userLocale');

        if (cookieLocale) {
            foundLocale = cookieLocale;
        } else if (urlParams.has('locale')) {
            foundLocale = urlParams.get('locale');
        } else {
            const lang = event.request.headers.get('accept-language')?.split(',')[0]
            if (lang) {
                foundLocale = lang;
            }
        }
        
        event.cookies.set('userLocale', foundLocale, {
            path: '/',  // Cookie path
            httpOnly: true,  // Prevents JavaScript access
            secure: true,  // Only send over HTTPS (in production)
            maxAge: 60 * 60 * 24 * 7, // Cookie expiration time (in seconds)
            sameSite: 'strict', // Cookie security policy
        });

        locale.set(foundLocale);
    }

    return await resolve(event);
}
