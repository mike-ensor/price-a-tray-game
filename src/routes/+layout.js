import {getActiveGame} from '$lib/index.js';

// /** @type {import('./$types').LayoutData} */

// i18 Imports & Libs
import {locale, waitLocale} from 'svelte-i18n'
import '$lib/i18n' // Import to initialize. Important :)
import {browser} from "$app/environment";


/** @type {import('./$types').PageLoad} */
export const load = async ({fetch, route}) => {

    if (browser) {
        locale.set(window.navigator.language);
    }

    await waitLocale();

    // Do not try to get the ActiveGame if displaying Ordering pages
    if (route.id.includes("(ordering)")) {
        return {};
    } else {
        return getActiveGame(fetch);
    }
}