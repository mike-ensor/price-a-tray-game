/*
* This page is used to mimic a order screen
*
*/
import {getGameItems} from "$lib/games/index.js";

export const load = async (event) => {
    const {fetch} = event;
    return {

        gameItems: await getGameItems(fetch,6),
        // Mock Orders
        // pork enchilada
        // cheese enchilada
        // steak enchilada
        // nuggies
        // fries
        // corn
        orders: [
            {
                id: 111,
                items: [
                    {
                        name: "Pork Enchilada",
                        quantity: 1,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    },
                    {
                        name: "Cheese Enchilada",
                        quantity: 1,
                        fulfilled: false,
                        label: 'cheese_enchilada'
                    },
                    {
                        name: "Steak Enchilada",
                        quantity: 1,
                        fulfilled: false,
                        label: 'steak_enchilada'
                    },
                    {
                        name: "Chicken Nuggets",
                        quantity: 1,
                        fulfilled: false,
                        label: 'nuggies'
                    },
                    {
                        name: "Fries",
                        quantity: 1,
                        fulfilled: false,
                        label: 'fries'
                    },
                    {
                        name: "Corn",
                        quantity: 1,
                        fulfilled: false,
                        label: 'corn'
                    }
                ]
            },
            {
                id: 112,
                items: [
                    {
                        name: "Fries",
                        quantity: 1,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    },
                    {
                        name: "Corn",
                        quantity: 1,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    },
                    {
                        name: "Doughnuts",
                        quantity: 1,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    }
                ]
            },
            {
                id: 113,
                items: [
                    {
                        name: "Burger",
                        quantity: 2,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    },
                    {
                        name: "Fries",
                        quantity: 1,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    },
                    {
                        name: "Tacos",
                        quantity: 2,
                        fulfilled: false,
                        label: 'pork_enchilada'
                    }
                ]
            }
        ]
    };
}
