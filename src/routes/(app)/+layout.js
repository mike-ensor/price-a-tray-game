
import {getActiveGame} from '$lib/index.js';

export const load = async (event) => {
    try {
        const {fetch} = event;
        return getActiveGame(fetch);
    } catch (err) {
        console.error(err);
        return {
            id: "NONE",
            name: "No Active Game",
            scoresRecorded: 0
        }
    }

}