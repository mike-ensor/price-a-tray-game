import {appEnvs} from "$lib/appEnvs.js";

async function activateGame(gameId) {
    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/game/activate/${gameId}`;
    const res = await fetch(url, {
        method: "PUT",
    });
    const gameStatus = res.json();

    return gameStatus.then((item) => {
        console.log("Game Update Status: " + item);
        return true;
    })
}

/** @type {import('./$types').Actions} */
export const actions = {
    default: async ({request}) => {

        const formData = await request.formData();

        const newActiveGameId = formData.get('activeGame');
        if(newActiveGameId) {
            console.log(`Switching active game to '${newActiveGameId}'`);
            return await activateGame(newActiveGameId).then((result) => {
                return result;
            });
        } else {
            console.log("No ActiveGame object, something isn't right");
            return false;
        }
    }
};