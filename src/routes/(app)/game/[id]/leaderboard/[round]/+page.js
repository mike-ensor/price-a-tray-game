import {appEnvs} from "$lib/appEnvs.js";
import lzString from 'lz-string';
import {browser} from "$app/environment"
import {MISSING_IMAGE} from '$lib/constants'

async function getRoundScore(roundId) {
    let round = {
        target: 0.00,
        finalScore: 0.00
    }

    try {
        const round_url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/game-rounds/${roundId}/final`;
        const round_res = await fetch(round_url);
        round = await round_res.json();
    } catch (e) {
        console.log("Exception during fetching round: " + e);
    }

    return round;
}

// get the place of this round
function getRoundPlace(leaderboard, roundId) {
    let place = -1;
    for(let i=0; i< leaderboard.length; i++) {
        if(leaderboard[i].game_round_id === roundId) {
            // we have a match!
            place = i+1;
            break;
        }
    }
    return place;
}

// may need this in the page.svelte since it is client-side
function getImageFromLocalStore(roundId) {
    if (browser) {
        const storageKey = `round_${roundId}`;
        const decompressed = lzString.decompressFromUTF16(localStorage.getItem(storageKey));
        return JSON.parse(decompressed);
    } else {
        return {
            id: roundId,
            imageData: MISSING_IMAGE
        };
    }
}

export const load = async (event) => {
    const {fetch, params} = event;

    const gameId = params.id;

    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games/${gameId}/leaderboard`;
    const res = await fetch(url);
    const leaderboard = await res.json();

    const game_url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games/${gameId}`;
    const game_res = await fetch(game_url);
    const game = await game_res.json();

    const roundId = params.round;
    const round = await getRoundScore(roundId);

    const roundPlaceObj = getRoundPlace(roundId);
    const roundPlace = roundPlaceObj.place;

    return {
        game,
        leaderboard,
        roundId,
        roundPlace: getRoundPlace(leaderboard, roundId),
        round,
        storedImage: getImageFromLocalStore(roundId)
    };

}