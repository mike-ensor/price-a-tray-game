import {appEnvs} from "$lib/appEnvs.js";

export const load = async (event) => {
    const {fetch} = event;
    const {params} = event;

    const id = params.id;
    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games/${id}/leaderboard`;
    const res = await fetch(url);
    const gameRounds = await res.json();

    const game_url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games/${id}`;
    const game_res = await fetch(game_url);
    const game = await game_res.json();

    return {
        game,
        gameRounds
    };

}