/*

APIs used:

GET /games/{id} -- Get a specific game
POST - /games/{game_id}/rounds -- Creates a new Round (ie, new player)

*/
import {appEnvs} from "$lib/appEnvs.js";
import {getCurrencyString} from "$lib/constants";
import {error} from "@sveltejs/kit";
import {getGameItems} from "$lib/games/index.js";

export const load = async (event) => {
    const {fetch} = event;
    const {params} = event;

    const id = params.id;

    async function getActiveGame(id) {
        const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games/${id}`;
        const activeGame = await fetch(url);

        if (!activeGame) {
            error(404, {
                message: 'Game is not found',
                code: 'NOT_FOUND'
            });
        }

        return await activeGame.json()
    }

    // Load the game information
    const activeGame = await getActiveGame(id);

    return {
        options: {
            gameItems: getGameItems(fetch, activeGame.model_id),
            game: {
                id,
                gameName: activeGame.name,
                interval: activeGame.round_length,
                locale: activeGame.locale
            },
            gamePlayer: await createRoundUser(event, activeGame)
        }
    };
}

/// This is called every time a page is loaded...could create a LOT of users...need to think about changing API
async function createRoundUser(event, game) {
    const {fetch} = event;
    const gameId = game.id;

    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games/${gameId}/rounds`;

    const newRoundObject = {
        game_id: `${gameId}`,
        game_runtime: 1,
        game_accelerator: 1,
        status: "PLAYING"
    };

    const requestBody = JSON.stringify(newRoundObject);

    const res = await fetch(url, {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: requestBody
    })

    const newRoundResponse = await res.json()

    return {
        id: newRoundResponse.id, /* NOT the player ID, this is the Game Round ID */
        playerId: newRoundResponse.player_id,
        target: newRoundResponse.target,
        targetInLocale: getCurrencyString(newRoundResponse.target, game.locale),
        // Initial score is the maximum possible, will update as game goes on
        score: {
            game_round_id: `${gameId}`,
            score: Number.MAX_VALUE
        }
    };
}

