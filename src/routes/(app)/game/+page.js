/*

PUT /games/{id} -- update a game
POST /games -- Create a new game
GET /games/active -- get the most recent active game (there may be more than 1, unlikely, but this will get 1)
GET /games/{id} -- Get a specific game
GET /games -- Get a list of all games

 */

import {appEnvs} from "$lib/appEnvs.js";

export const load = async (loadEvent) => {
    const { fetch } = loadEvent;
    const url = `http://${appEnvs.GAME_API_HOST}:${appEnvs.GAME_API_PORT}/games`;
    const res_games = await fetch(url);
    const games = await res_games.json();

    return {
        games
    };
}
