import {json} from "@sveltejs/kit";

export const GET = () => {
    // TODO: Look for some sort of status items (like connection to DB and such)

    const result = {
        "status": "ok"
    }

    return json(result);
}