# Overview

Refer to this file in your App Package. Suggest the following Patches with Kustomization

## Patches

This frontend app is run in a browser where the K8s DNS does not resolve, therefore the 
dependent services need to either have a `LoadBalancer` service with an IP on the same 
subnet, or use an `Ingress` where the DNS name is referenced to a Private IP (CNAME).

```yaml
# ...
# ... kustomization.yaml file
# ...
patches:
  - target:
      kind: ConfigMap
      labelSelector: "app-config=price-a-tray-frontend"
      namespace: price-a-tray
    patch: |-
      - op: replace
        path: /data/PUBLIC_RTSP_API_HOST
        value: "ip-or-resolvable-dns-name"
      - op: replace
        path: /data/PUBLIC_INFERENCE_API_HOST
        value: "ip-or-resolvable-dns-name"
      - op: replace
        path: /data/PUBLIC_GAME_API_HOST
        value: "ip-or-resolvable-dns-name"
```