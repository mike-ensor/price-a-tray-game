# Overview

This is a fun game using Object Detection on a trained set of objects in real-time. The goal of the game is
to create the combination of objects on the tray to be as close to the target as possible within the configurable
timeframe.

Architecture diagram can be found [here](docs/architecture.md)

## Development

Configuration Files needed
1. `.npmrc` with authentication to the GitLab Common Svelte components package repository
2. `.envrc` with ENV variables for the dependencies

### Running

```shell
npm run dev
```

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/price-a-tray-dynamic-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-backend
: This is the Game API where rules, game lifecycle, scores and data are stored

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/price-a-tray-game-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This project provides an API to inference extraced frames (image) against the edge model pulled from the GCS bucket.

