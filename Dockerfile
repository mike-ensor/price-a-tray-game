# Stage 1: Install dependencies and build the project
# Use a Node.js base image  # as build-stage
FROM node:18-alpine 

# Set the working directory
WORKDIR /app

# Copy the package.json and package-lock.json (if available)
COPY package*.json ./

RUN --mount=type=secret,id=npmrc,target=/root/.npmrc npm i @mike-ensor/common-project-svelte-components --scope=@mike-ensor

# Copy the rest of the source code
COPY . .

# Install dependencies
RUN --mount=type=secret,id=npmrc,target=/root/.npmrc npm install

# Build the app
RUN npm run build

# Expose the port the app runs on
EXPOSE 4173

# Set the command to run the app
CMD ["npm", "run", "preview"]
